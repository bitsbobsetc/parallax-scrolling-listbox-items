﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Microsoft.Phone.Controls;

namespace ParallaxLongListSelector
{
    public partial class ParallaxLongListItem : UserControl
    {
        private LongListSelector _lls;
        private double _llsHeight;

        public ParallaxLongListItem()
        {
            InitializeComponent();

            Loaded += OnLoaded;
        }

        public static readonly DependencyProperty SourceProperty = DependencyProperty.Register(
            "Source", typeof(ImageSource), typeof(ParallaxLongListItem), new PropertyMetadata(default(ImageSource)));

        public ImageSource Source
        {
            get { return (ImageSource)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            UpdateMargin();
        }

        public static readonly DependencyProperty ScrollExtentProperty = DependencyProperty.Register(
            "ScrollExtent",
            typeof(double),
            typeof(ParallaxLongListItem),
            new PropertyMetadata(0.0));

        public double ScrollExtent
        {
            get { return (double)GetValue(ScrollExtentProperty); }
            set { SetValue(ScrollExtentProperty, value); }
        }

        public static readonly DependencyProperty ScrollMarginProperty = DependencyProperty.Register(
            "ScrollMargin",
            typeof(double),
            typeof(ParallaxLongListItem),
            new PropertyMetadata(OnScrollChanged));

        public double ScrollMargin
        {
            get { return (double)GetValue(ScrollMarginProperty); }
            set { SetValue(ScrollMarginProperty, value); }
        }
        
        private static void OnScrollChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = d as ParallaxLongListItem;

            if (control != null) 
                control.UpdateMargin();
        }

        public void UpdateMargin()
        {
            //Determin the height of the containing LongListSelector
            if (_lls == null)
            {
                _lls = ElementExtensions.FindParent<LongListSelector>(this);
                _llsHeight = _lls.ActualHeight;
            }

            //Get the current point of this List item relative to the top of the parent LongListSelector control
            var relativePoint = InnerImage.TransformToVisual(_lls).Transform(new Point(0, 0));

            //If this point is visible (ie. is within the bounds of the LongListSelector) then we can update its transform
            var vm = DataContext as BitmapImage;
            if (vm == null) 
                return;

            var imageHeight = vm.PixelHeight;

            if (((relativePoint.Y > (imageHeight * -1)) || imageHeight == 0) && 
                (relativePoint.Y <= _llsHeight))
            {
                //The transform margin value will start at the maximum negative margin and scroll back to 0;
                var margin = (ScrollExtent * -1) + (relativePoint.Y / _llsHeight) * ScrollExtent;

                InnerImage.RenderTransform = new TranslateTransform {X = 0, Y = margin};
            }
        }
    }
}
